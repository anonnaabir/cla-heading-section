<?php

/**
 * Plugin Name:       CAM Heading Section
 * Plugin URI:        http://pixelomatic.com/
 * Description:       This plugin add custom heading section for wpbackery plugin.
 * Version:           2.1
 * Author:            Pixelomatic Team
 * Author URI:        http://pixelomatic.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       cla-heading-section
 * Domain Path:       /languages
 */

// don't load directly
if (!defined('ABSPATH')) {
    die('-1');
}

/**
 * necessary hook when plugin loaded
 */
function cla_heading_plugins_loaded()
{
    // check if js_composer installed and activated
    if (!defined('WPB_VC_VERSION')) {
        add_action('admin_notices', 'cla_heading_admin_notice_missing_main_plugin');
        return;
    }
}
add_action('plugins_loaded', 'cla_heading_plugins_loaded');

/**
 * Admin notice
 *
 * Warning when the site doesn't have js_composer installed or activated.
 *
 * @since 1.0.0
 *
 */
function cla_heading_admin_notice_missing_main_plugin()
{
    if (isset($_GET['activate'])) unset($_GET['activate']);

    $message = sprintf(
        /* translators: 1: Plugin name 2: Elementor */
        esc_html__('%1$s requires %2$s to be installed and activated.', 'cla-heading-section'),
        '<strong>' . esc_html__('CAM Heading Section', 'cla-heading-section') . '</strong>',
        '<strong>' . esc_html__('WPBackery Page Builder', 'cla-heading-section') . '</strong>'
    );

    printf('<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message);
}

/**
 * WPBackery vc_map function
 */
function cla_heading_section_element_integrateWithVC()
{
    vc_map(array(
        "name" => __("CAM Heading", "cla-heading-section"),
        "base" => "cla_heading_section",
        "show_settings_on_create" => true,
        "params" => array(
            array(
                "type" => "textarea_html",
                "heading" => __("Content", "cla-heading-section"),
                "param_name" => "content", // Important: Only one textarea_html param per content element allowed and it should have "content" as a "param_name"
                "value" => __("<p>I am test text block. Click edit button to change this text.</p>", "cla-heading-section"),
                "description" => __("Enter your content.", "cla-heading-section")
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Element Align', 'cla-heading-section'),
                'description' => __('Select element align.', 'cla-heading-section'),
                'param_name' => 'element_align',
                'value' => array(
                    esc_html__('Left', 'cla-heading-section') => 'left',
                    esc_html__('Center', 'cla-heading-section') => 'center',
                    esc_html__('Right', 'cla-heading-section') => 'right',
                ),
                'std' => 'left'
            ),
            array(
                'type' => 'vc_link',
                'heading' => __('URL (Link)', 'js_composer'),
                'param_name' => 'btn_link',
                'description' => __('Add link to button.', 'js_composer'),
                // compatible with btn2 and converted from href{btn1}
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Button Class Name', 'cla-heading-section'),
                'param_name' => 'btn_class',
                'description' => esc_html__('Style button differently - add a class name refer to it in custom css.', 'cla-heading-section'),
            ),

            array(
                'type' => 'dropdown',
                'heading' => __('Title Tag', 'cla-heading-section'),
                'description' => __('Select html title tag.', 'cla-heading-section'),
                'param_name' => 'title_tag',
                'group' => 'Title',
                'value' => array(
                    esc_html__('H1', 'cla-heading-section') => 'h1',
                    esc_html__('H2', 'cla-heading-section') => 'h2',
                    esc_html__('H3', 'cla-heading-section') => 'h3',
                    esc_html__('H4', 'cla-heading-section') => 'h4',
                    esc_html__('H5', 'cla-heading-section') => 'h5',
                    esc_html__('H6', 'cla-heading-section') => 'h6',
                ),
                'std' => 'h2'
            ),

            array(
                'type' => 'textarea',
                'heading' => esc_html__('Title', 'cla-heading-section'),
                'param_name' => 'text',
                'admin_label' => true,
                'value' => esc_html__('This is custom heading element', 'cla-heading-section'),
                'description' => esc_html__('Note: If you are using non-latin characters be sure to activate them under Settings/WPBakery Page Builder/General Settings.', 'cla-heading-section'),
                'group' => 'Title'
            ),
            
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Font Size', 'cla-heading-section'),
                'param_name' => 'title_font_size',
                'description' => esc_html__('Change title font size.', 'cla-heading-section'),
                'value' => '30px',
                'group' => 'Title'
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Title Color', 'cla-heading-section'),
                'param_name' => 'title_color',
                'description' => esc_html__('Change title color', 'cla-heading-section'),
                'std' => '#000000',
                'group' => 'Title'
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Link', 'cla-heading-section'),
                'param_name' => 'title_link',
                'description' => esc_html__('Link for the title.', 'cla-heading-section'),
                'group' => 'Title'
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Extra class name', 'cla-heading-section'),
                'param_name' => 'title_class',
                'description' => esc_html__('Style title of element differently - add a class name and refer to it in custom CSS.', 'cla-heading-section'),
                'group' => 'Title'
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__('Subtitle', 'cla-heading-section'),
                'param_name' => 'subtext',
                'admin_label' => true,
                'value' => esc_html__('This is custom heading element', 'cla-heading-section'),
                'description' => esc_html__('Note: If you are using non-latin characters be sure to activate them under Settings/WPBakery Page Builder/General Settings.', 'cla-heading-section'),
                'group' => 'Subtitle'
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Font Size', 'cla-heading-section'),
                'param_name' => 'subtitle_font_size',
                'description' => esc_html__('Change subtitle font size.', 'cla-heading-section'),
                'value' => '24px',
                'group' => 'Subtitle'
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__('Title Color', 'cla-heading-section'),
                'param_name' => 'subtitle_color',
                'description' => esc_html__('Change subtitle color', 'cla-heading-section'),
                'std' => '#000000',
                'group' => 'Subtitle'
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__('Extra class name', 'cla-heading-section'),
                'param_name' => 'subtitle_class',
                'description' => esc_html__('Style subtitle of element differently - add a class name and refer to it in custom CSS.', 'cla-heading-section'),
                'group' => 'Subtitle'
            ),
            array(
                'type' => 'css_editor',
                'heading' => __( 'Css', 'cla-heading-section' ),
                'param_name' => 'css',
                'group' => __( 'Design options', 'cla-heading-section' ),
            ),
        )
    ));
}
add_action('vc_before_init', 'cla_heading_section_element_integrateWithVC');

/**
 * Shortcode for CAM Heading Element
 */
function cla_heading_section_shortcode($atts, $content = null)
{
    ob_start();
    $atts = shortcode_atts(array(
        'element_align' => 'left',
        'btn_link' => '',
        'btn_class' => '',
        'text' => esc_html__('This is custom heading element', 'cla-heading-section'),
        'title_tag' => 'h2',
        'title_font_size' => '30px',
        'title_color' => '#000000',
        'title_link' => '',
        'title_class' => '',
        'subtext' => esc_html__('This is custom heading element', 'cla-heading-section'),
        'subtitle_font_size' => '24px',
        'subtitle_color' => '#000000',
        'subtitle_class' => '',
        'css' => '',
    ), $atts);

    wp_enqueue_style('cla_heading_section', plugins_url('/assets/css/cla_heading.css', __FILE__));
    $el_id = uniqid('cla_heading_');
    $btn_class = !empty($atts['btn_class']) ? ' ' . $atts['btn_class'] : '';
    $title_font_size = get_font_size($atts['title_font_size']);
    $title_class = !empty($atts['title_class']) ? ' ' . $atts['title_class'] : '';
    $subtitle_font_size = get_font_size($atts['subtitle_font_size']);
    $subtitle_class = !empty($atts['subtitle_class']) ? ' ' . $atts['subtitle_class'] : '';
    $content = wpb_js_remove_wpautop($content, true);
    $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $atts['css'], ' ' ), 'cla_heading_section', $atts );

    $style = "
        #{$el_id} .subtitle { font-size: {$subtitle_font_size}; color: {$atts['subtitle_color']} }
        #{$el_id} .title { font-size: {$title_font_size}; color: {$atts['title_color']} }
    ";
    wp_add_inline_style('cla_heading_section', $style);
    $target = $rel = $url = $btn_title = '';
    if (isset($atts['btn_link'])) {
        $vc_link = vc_build_link($atts['btn_link']);
        if (strlen($vc_link['target'])) {
            $target = ' target="' . esc_attr($vc_link['target']) . '"';
        }
        if (strlen($vc_link['rel'])) {
            $rel = ' rel="' . esc_attr($vc_link['rel']) . '"';
        }
        if( strlen($vc_link['url']) ) {
            $url = esc_url($vc_link['url']);
        }
        if( strlen($vc_link['title']) ) {
            $btn_title = esc_html($vc_link['title']);
        }
    }

?>
    <div id="<?php echo $el_id; ?>" class="section_heading_wrapper <?php echo 'text-' . $atts['element_align']; echo ' ' . $css_class; ?>">
        <?php if (!empty($atts['subtext'])) { ?><h2 class="subtitle<?php echo $subtitle_class; ?>"><?php echo $atts['subtext']; ?></h2><?php } ?>
        <?php if (!empty($atts['text'])) { 
            if(!empty($atts['title_link'])) { echo '<a href="'.esc_url($atts['title_link']).'">'; } 
        ?>

        <?php echo '<'.$atts['title_tag'].' class="title'.$title_class.'">'.$atts['text'].'</'.$atts['title_tag'].'>' ;?>

        <?php if( !empty($atts['title_link']) ) { echo '</a>'; } 
    } ?>
        <?php if (!empty($content)) { ?><div class="content"><?php echo $content; ?></div><?php } ?>
        <?php if (!empty($url)) : ?>
            <a href="<?php echo $url; ?>" class="site_cta<?php echo $btn_class; ?>"<?php echo $target . $rel; ?>><?php echo $btn_title; ?></a>
        <?php endif; ?>
    </div>
<?php

    return ob_get_clean();
}
add_shortcode('cla_heading_section', 'cla_heading_section_shortcode');
/**
 * helper functions
 */
function get_font_size($value)
{
    $pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
    preg_match($pattern, $value, $matches);
    $font_size = isset($matches[1]) ? (float) $matches[1] : (float) $value;
    $unit = isset($matches[2]) ? $matches[2] : 'px';
    return $font_size = $font_size . $unit;
}
